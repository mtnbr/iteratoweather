<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WeatherController extends Controller
{
    /**
     * @param Request $request
     *
     * Gets The Input From Main App and Returns A Result as Json
     *
     * @return mixed
     */
    public function weatherCheck ( Request $request )
    {
        $request->validate( [ 'key' => 'required', 'city' => 'required' ] );

        $key = $request->get( 'key' );
        //$key = '46708d7bed7b107043307af57ce9238e';

        $city = $request->get( 'city' );

        $weatherData = $this->getCityWeather( $key, $city );

        return response( $weatherData, 200 )->header( 'Content-Type', 'text/plain' );
    }

    /**
     * @param $key
     * @param $city
     *
     * Crawl the api and get the result for given city
     *
     * @return mixed
     */
    public function getCityWeather ( $key, $city )
    {
        $url = "http://api.openweathermap.org/data/2.5/weather?q=$city&appid=$key";

        $curl = curl_init();
        curl_setopt_array( $curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL            => $url,
            CURLOPT_USERAGENT      => 'Codular Sample cURL Request'
        ] );

        $resp = curl_exec( $curl );

        curl_close( $curl );

        return $resp;
    }
}
