<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Iterato Weather App</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="/css/app.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="text-center p-4">
    <h1>Iterato Weather App</h1>
</div>
<div class="row">
    <form class="col-lg-4" style="margin: auto;">
        <div>
            <input name="key" class="form-control" type="text" placeholder="API Key">
        </div>
        <div class="pt-3">
            <div class="input-group">
                <input name="city" class="form-control" type="text" placeholder="City">
                <span class="absolution"><button class="submit-button" type="button">&#10003;</button></span>
            </div>
        </div>
    </form>
</div>

<div class="row weather p-5">
</div>

<script src="/js/app.js"></script>

<script>
    $(document).ready(function () {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $(".submit-button").click(function () {
            $.ajax({
                url: '/city-detail',

                type: 'POST',

                data: {_token: CSRF_TOKEN, key: $('input[name="key"]').val(), city: $('input[name="city"]').val()},

                dataType: 'JSON',
                success: function (data) {

                    console.log(data)

                    $(".weather").prepend('<div class="col-3"><p style="border-bottom: 1px solid grey">' + data.name + '</p>' + data.main.temp + '\n' +
                        '&#8451;</div>');
                }
            });
        });
    });
</script>
</body>
</html>